﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LV5
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IShipable> list = new List<IShipable>();
            Box box = new Box("Order 1");
            Product product1 = new Product("Acer Laptop", 59.99, 2.35);
            Product product2 = new Product("Huawei P30", 19.99, 0.36);
            list.Add(box);
            list.Add(product1);
            list.Add(product2);

            ShippingService price = new ShippingService(3.50);
            double totalWeight = 0;
            foreach (IShipable ship in list)
            {
                totalWeight += ship.Weight;
                Console.WriteLine(ship.Description());
            }
            Console.WriteLine(price.ToString() + price.Price(totalWeight) + " euro");
            Console.ReadLine();
        }
    }
}
