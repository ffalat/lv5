﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LV5
{
    class ShippingService
    {
        private double pricePerKilogram;

        public ShippingService(double pricePerKilogram) { this.pricePerKilogram = pricePerKilogram; }

        public double Price(double weight)
        {
            return pricePerKilogram * weight;
        }

        public override string ToString()
        {
            return "Total price of delivery is: ";
        }
    }
}
